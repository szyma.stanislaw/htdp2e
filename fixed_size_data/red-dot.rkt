;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname red-dot) (read-case-sensitive #t) (teachpacks ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp")) #f)))
(define MTS (empty-scene 100 100))
(define DOT (circle 3 "solid" "red"))

; a Posn represents the state of the world.

; Posn -> Posn
(define (main p0)
  (big-bang p0
    [on-tick x+]
    [on-mouse reset-dot]
    [to-draw scene+dot]))

; Posn -> Image
; adds a red dot to MTS at p
(define (scene+dot p)
  (place-image DOT (posn-x p) (posn-y p) MTS))

(check-expect (scene+dot (make-posn 10 20))
               (place-image DOT 10 20 MTS))
(check-expect (scene+dot (make-posn 50 50))
              (place-image DOT 50 50 MTS))


; Design the function posn-up-x, which consumes a
; Posn p and a Number n. It produces a Posn like p
; with n in the x field.
(check-expect (posn-up-x (make-posn 15 20) 20)
              (make-posn 20 20))

(define (posn-up-x p n)
  (make-posn n (posn-y p)))

; Posn -> Posn
; increases the x-coordinate of p by 3
(define (x+ p)
  (posn-up-x p (+ (posn-x p) 3)))

(check-expect (x+ (make-posn 20 20))
              (make-posn 23 20))

; MouseEvent Posn -> Posn
; when the mouse is clicked dot is reset to its
; original position
(check-expect
 (reset-dot (make-posn 23 89) 90 90 "button-down")
              (make-posn 90 90))
(check-expect (reset-dot
               (make-posn 23 89) 90 90 "button-up")
              (make-posn 23 89))

(define (reset-dot p x y me)
  (cond
    [(mouse=? "button-down" me) (make-posn x y)]
    [else p]))





              