;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname ex12) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
(define (cvolume x)
  (string-append "the volume of such cube is: " (number->string (expt x 3))))

(define (csurface x)
  (string-append "the area of such square is: "(number->string (expt x 2))))