#lang htdp/isl+

(require 2htdp/image)
(require 2htdp/abstraction)
(require 2htdp/universe)

; PuzzleState -> PuzzleState
; is the final state reachable from state0
; generative creates a tree of possible boat rides 
; termination ???
(check-expect (solve initial-puzzle) final-puzzle)
 
(define (solve state0)
  (local (; [List-of PuzzleState] -> PuzzleState
          ; generative generates the successors of los
          (define (solve* los)
            (cond
              [(ormap final? los)
               (river-prev (first (filter final? los)))]
              [else
               (solve* (create-next-states los))])))
    (solve* (list state0))))

;; ex. 521

; PuzzleState.v2 is a structure:
(define-struct river [left boat right prev])
; interpretation:
;   (make-river Ratio Dir Ratio [List-of PuzzleState])
; specifies the number of missionaries
; and cannibals on each side of the river
; and the current position of the boat
; in terms of a left or right river bank
; data accumulator (ex. 522):
; prev represents number of previous steps
; taken in order to get to the current
; PuzzleState

; Dir is either 'left or 'right

; Ratio is a structure:
(define-struct ratio [mis can])
; interpretation:
;  (make-ratio N N)
; determines the number of missionaries
; and cannibals on a given river bank


; examples for PuzzleState.v1 !!
(define initial-puzzle (make-river (make-ratio 3 3)
                         'left
                         (make-ratio 0 0)
                         '()))

(define inter-puzzle (make-river (make-ratio 1 1)
                          'left
                          (make-ratio 2 2)
                          '()))

(define final-puzzle (make-river (make-ratio 0 0)
                        'right
                        (make-ratio 3 3)
                        '()))

; PuzzleState.v2 -> Boolean
; are all the people on the right river bank
(define (final? p)
  (equal? (river-left p) (make-ratio 0 0)))

; PuzzleState.v2 -> Image
(define (render-mc p)
  (local ((define m (circle SIZE 'solid 'black)) ; a missionary
          (define c (circle SIZE 'solid 'white)) ; a cannibal
          (define boat (square (* SIZE 2) 'solid 'black))
          (define river (rectangle (* SIZE 10) (* SIZE 10) 'solid 'lightblue))
          (define bank (rectangle (* SIZE 5) (* SIZE 10) 'solid 'darkyellow))

          ; Ratio -> Image
          (define (bank-render r)
            (beside
             (foldr above empty-image
                    (build-list (ratio-mis r) (lambda (n) m)))
             (foldr above empty-image
                    (build-list (ratio-can r) (lambda (n) c)))))

          ; PuzzleState.v2 -> Image
          (define (river-render p)
            (overlay/align (river-boat p) 'middle
                           boat
                           river))

          ; PuzzleState.v2 -> Image
          (define (scene-render p)
            (beside (overlay (bank-render (river-left p))
                             bank)
                    (river-render p)
                    (overlay (bank-render (river-right p))
                             bank))))
    (scene-render p)))

(define SIZE 5) ; radius of a circle representing a missionary or cannibal



;; ex. 523

; [List-of PuzzleState] -> [List-of PuzzleState]
(define (create-next-states los)
  (local (; [List-of PuzzleState] -> [List-of PuzzleState]
          ; generates continuation of a game for every state
          (define (generate-next-states los)
            (foldr (lambda (c rst) (if (member? c (river-prev c))
                                       rst
                                       (append (state-generate c) rst)))
                   '() los))
          
          ; PuzzleState -> [List-of PuzzleState]
          ; stores only the acceptable states
          (define (state-generate p)
            (filter not-eaten? (generate-all-states p)))
          
          ; PuzzleState -> Boolean
          ; does p fulfill the game rules
          (define (not-eaten? p)
            (local ((define left (river-left p))
                    (define right (river-right p))
                    (define lm (ratio-mis left))
                    (define lc (ratio-can left))
                    (define rm (ratio-mis right))
                    (define rc (ratio-can right)))
              (and (= (+ lm lc rm rc) 6)
                   (andmap (lambda (n) (>= n 0))
                           (list lm lc rm rc))
                   (or (and (>= lm lc) (>= rm rc))
                       (or (= lm 0) (= rm 0))))))
          
          ; PuzzleState -> [List-of PuzzleState]
          ; creates all possible continuation of p
          (define (generate-all-states p)
            (map (lambda (c) (generate-state p (first c) (second c)))
                 list-of-combinations))
          
          ; generates all possible combinations
          ; of transporting two people on a boat
          (define list-of-combinations
            (filter (lambda (c) (< 0 (+ (first c) (second c)) 3))
                    (for*/list ([m 3] [c 3])
                      (list m c))))
          
          ; PuzzleState N N-> PuzzleState
          ; given number of missionaries
          ; and cannibals going by a boat
          ; produces a next state
          (define (generate-state p m c)
            (local ((define left (river-left p))
                    (define right (river-right p))
                    (define dir->num
                      (if (equal? 'left (river-boat p)) 1 -1))
                    (define prev (river-prev p))
                    (define prev-removed
                      (make-river left (river-boat p) right '()))
                    (define lm (ratio-mis left))
                    (define lc (ratio-can left))
                    (define rm (ratio-mis right))
                    (define rc (ratio-can right)))
              (make-river
               (make-ratio
                (- lm (* m dir->num)) (- lc (* c dir->num)))
               (if (= dir->num 1) 'right 'left)
               (make-ratio
                (+ rm (* m dir->num)) (+ rc (* c dir->num)))
               (cons prev-removed prev)))))
    
    (generate-next-states los)))
  
;; ex. 525

; renders one of the solutions
(define (visualise p)
  (run-movie 2 (map render-mc (reverse (solve p)))))
