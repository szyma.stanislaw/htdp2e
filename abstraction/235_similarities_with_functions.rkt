#lang htdp/isl

; Los string -> Boolean
; looks for a string in a list
(define (contains? s los)
  (cond
    [(empty? los) #f]
    [else (or (string=? s (first los))
              (contains? s (rest los)))]))

(check-expect (contains? "yes" '("no" "no" "yes")) #t)

; Los -> Boolean
; does a list contain atom
(define (contains-atom? los)
  (contains? "atom" los))

; Los -> Boolean
; does a list contain basic
(define (contains-basi? los)
  (contains? "basic" los))

; Los -> Boolean
; looks for zoo in a list
(define (contains-zoo? los)
  (contains? "zoo" los))
