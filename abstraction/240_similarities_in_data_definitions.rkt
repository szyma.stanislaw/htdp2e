#lang htdp/isl

(define-struct layer [stuff])

; An LStr is one of:
; - String
; - (make-layer LStr)

(define lstr0 "yes")
(define lstr1 (make-layer "hello"))
(define lstr2 (make-layer (make-layer "hello")))

; An LNum is one of:
; - Number
; - (make-layer LNum)

(define lnum0 44)
(define lnum1 (make-layer 2))
(define lnum2 (make-layer (make-layer 3)))

; An [List-of X] is one of:
; - X
; - (make-layer [List-of X])

; An Lstr is the same as [List-of String] and
; Lnum is the same as [List-of Number]
