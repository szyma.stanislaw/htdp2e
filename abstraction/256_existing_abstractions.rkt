#lang htdp/isl

; [X] [X -> Number] [NEList-of X] -> X 
; finds the (first) item in lx that maximizes f
; if (argmax f (list x-1 ... x-n)) == x-i, 
; then (>= (f x-i) (f x-1)), (>= (f x-i) (f x-2)), ...
; (define (argmax f lx) ...)

(argmax sqrt (list 1 2 3 4 5)) ;== 5
(argmax string-length
        (list "y" "ye" "yes" "yeeeees")) ;== "yeeeees"

(argmin sqrt (list 1 2 3 4 5)) ;== 1
