#lang htdp/isl+

(require 2htdp/image)

;;;;;;;;;;; ex. 267 ;;;;;;;;;;;;;;

; Use map to define the function convert-euro,
; which converts a list of US$ amounts into a list of
; € amounts based on an exchange rate of US$1.06 per €
; (on April 13, 2017).

; [List-of Number] -> [List-of Number]
; converts usd to eur for every element on l

(check-expect
 (convert-euro '(1 2 3 4 5))
 '(1.06 2.12 3.18 4.24 5.30))

(define (convert-euro lon)
  (local (; Number -> Number
          ; applies current exchange rate
          (define (exchange n)
            (* n RATE)))
    (map exchange lon)))

(define RATE 1.06) ; US$1.06 per € (on April 13, 2017)


; Also use map to define convertFC, which converts a list of
; Fahrenheit measurements to a list of Celsius measurements.

; [List-of Number] -> [List-of Number]
; converts fahrenheit to celsius for every
; element on l

(check-expect
 (convertFC '(50 95 68))
 '(10 35 20))

(define (convertFC lon)
  (local (; Number -> Number
          ; Fahrenheit to Celsius
          (define (F->C n)
            (* (- n 32) 5/9)))
    (map F->C lon)))


; Finally, try your hand at translate, a function that
; translates a list of Posns into a list of lists of
; pairs of numbers.

; Pair is a (list Number Number)

; LLP is one of:
; - '()
; (cons Pair LLP)

; [List-of Posn] -> [List-of LLP]
; converts a list of Posns to LLP

(check-expect
 (translate (list (make-posn 20 20) (make-posn 80 80)))
 (list (list 20 20) (list 80 80)))

(define (translate lop)
  (local (; Posn -> Pair
          ; translates a Posn to Pair
          (define (create-pair p)
            (list (posn-x p) (posn-y p))))
    (map create-pair lop)))


;;;;;;;;;;;;; ex. 268 ;;;;;;;;;;;;;;;;;;

; An inventory record specifies the name of an item,
; a description, the acquisition price, and the
; recommended sales price. Define a function that
; sorts a list of inventory records by the difference
; between the two prices.

; An inventory record is a structure:
(define-struct ir (name spec ap rsp))
; (make-ir String String Number Number)
; specifies the name of an item, its description and
; also the acquisition and the recommended sales price

; [List-of IR] -> [List-of IR]
; sorts the inventory record by the difference
; between acquisition and recommended sales price
; in ascending order

(check-expect
 (sort-price (list (make-ir "car" "yes" 3 4)
                   (make-ir "doll" "no" 2 5)))
 (list (make-ir "doll" "no" 2 5)
       (make-ir "car" "yes" 3 4)))

(define (sort-price loi)
  (local (; IR -> Number
          ; calculates the difference
          ; between ap and rsp
          (define (dprice i)
            (- (ir-ap i) (ir-rsp i)))
          
          ; IR IR -> Boolean
          ; is the dprice for the first
          ; item smaller than the second?
          (define (dprice<? x y)
            (< (dprice x) (dprice y))))
    (sort loi dprice<?)))


;;;;;;;;;;;;; ex. 269 ;;;;;;;;;;;;;;;;;;

; Define eliminate-expensive. The function consumes a number,
; ua, and a list of inventory records, and it produces a list of
; all those structures whose sales price is below ua.

; Number [List-of IR] -> [List-of IR]
; filters out all the items whose sales price is above ua

(check-expect
 (eliminate-expensive 5 (list (make-ir "car" "yes" 3 5)
                              (make-ir "doll" "yes" 4 8)))
 (list (make-ir "car" "yes" 3 5)))

(define (eliminate-expensive ua loi)
  (local (; IR -> Boolean
          ; is the sales price of IR
          ; smaller than ua?
          (define (cheap? i)
            (<= (ir-rsp i) ua)))
    (filter cheap? loi)))

; Then use filter to define recall, which consumes the name of an
; inventory item, called ty, and a list of inventory records and
; which produces a list of inventory records that do not use the name ty.

; String [List-of IR] -> [List-of IR]
; produces a list of all inventory records
; that do not contain s

(check-expect
 (recall "car" (list (make-ir "car" "yes" 3 5)
                     (make-ir "doll" "yes" 4 8)))
 (list (make-ir "doll" "yes" 4 8)))

(define (recall s loi)
  (local (; IR -> Boolean
          ; s is not the name of that item?
          (define (notyou? i)
            (not (string=? (ir-name i) s))))
    (filter notyou? loi)))

; In addition, define selection, which consumes two lists of
; names and selects all those from the second one that are
; also on the first.

; [List-of String] [List-of String] -> [List-of String]
; selects all the items in b that are in a

(check-expect
 (selection (list "yes" "maybe")
            (list "yes" "noo" "maybe"))
 (list "yes" "maybe"))

(define (selection a b)
  (local (; String -> Boolean
          ; is it in a?
          (define (present? s)
            (member? s a)))
    (filter present? b)))


;;;;;;;;;;;;; ex. 270 ;;;;;;;;;;;;;;;;;;;

; Use build-list to define a function that

; Number -> [List-of Number]
; 1. creates the list (list 0 ... (- n 1)) for any natural number n;

(check-expect (countfrom0 5) (list 0 1 2 3 4))

(define (countfrom0 n)
  (local (; Number -> Number
          ; does nothing
          (define (f x) x))
    (build-list n f)))

; Number -> [List-of Number]
; 2. creates the list (list 1 ... n) for any natural number n;

(check-expect (count 5) '(1 2 3 4 5))

(define (count n)
  (build-list n add1))

; Number -> [List-of Number]
; 3. creates the list (list 1 1/2 ... 1/n) for any natural number n;

(check-expect (count-rec 5) '(1 1/2 1/3 1/4 1/5))

(define (count-rec n)
  (local (; Number -> Number
          ; creates a reciprocal of n+1
          (define (reciprocal n)
            (/ 1 (add1 n))))
    (build-list n reciprocal)))

; Number -> [List-of Number]
; 4. creates the list of the first n even numbers;

(check-expect (count-even 5) '(0 2 4 6 8))

(define (count-even n)
  (local (; Number -> Number
          ; doubles n
          (define (double n)
            (* n 2)))
    (build-list n double)))

; Number -> [List-of LoN]
; creates a diagonal square of 0s and 1s;

; LoN is one of:
; - '()
; (cons Number LoN)

(check-expect
 (idmatrix 3) '((1 0 0)
                (0 1 0)
                (0 0 1)))

(define (idmatrix n)
  (local (; Number -> LoN
          ; creates a row of an identity matrix
          ; while preserving the no. of columns
          (define (gen-row row col)
            (cond
              [(= col n) '()]
              [else
               (cons (if (= col row) 1 0)
                     (gen-row row (add1 col)))]))

          ; helper function
          (define (create-row n)
            (gen-row n 0)))
    (build-list n create-row)))


;;;;;;;;;;;; ex. 271 ;;;;;;;;;;;;;;;;;;

; Use ormap to define find-name. The function consumes a name and
; a list of names. It determines whether any of the names on the
; latter are equal to or an extension of the former.

; String [List-of String] -> Boolean
; determines if any element on l contains s

(check-expect
 (find-name "yes" '("yes" "yyyyes" "no")) #t)

(define (find-name name los)
  (local (; String String -> Boolean
          ; does the second string contain the first one
          (define (contains? s)
            (string-contains? name s)))
    (ormap contains? los)))

; With andmap you can define a function that checks all names on
; a list of names that start with the letter "a"

; [List-of String -> Boolean
; determines whether all names start with "a"

(check-expect
 (onlyadams? '("adam" "adam" "aa")) #t)

(define (onlyadams? los)
  (local (; String -> Boolean
          ; does s start with "a"
          (define (adam? s)
            (equal? "a" (substring s 0 1))))
    (andmap adam? los)))

; Should you use ormap or andmap to define a function that
; ensures that no name on some list exceeds a given width?

; [List-of String] Number -> Boolean
; determines that all strings in l are shorter than n

(check-expect
 (size-matters 2 '("ye" "no" "yees")) #f)

(define (size-matters n los)
  (local (; String -> Boolean
          ; no longer than n?
          (define (good? s)
            (<= (string-length s) n)))
    (andmap good? los)))


;;;;;;;;;;;;; ex. 272 ;;;;;;;;;;;;;;;;;;

; Recall that the append function in ISL concatenates the items of two lists or,
; equivalently, replaces '() at the end of the first list with the second list.
; Use foldr to define append-from-fold.

; [List-of X] [List-of X] -> [List-of X]
; appends two lists

(check-expect
 (append-from-fold (list 1 2 3) (list 4 5 6))
 (list 1 2 3 4 5 6))

(define (append-from-fold l1 l2)
  (local(; X -> [List-of X]
         (define (plug x l)
           (cons x l)))
    (foldr plug l2 l1)))

; Now use one of the fold functions to define functions that compute
; the sum and the product, respectively, of a list of numbers.

; [List-of Number] -> Number
; adds all numbers in l

(check-expect
 (sum '(1 2 3)) 6)

(define (sum lon)
  (foldr + 0 lon))

; [List-of Number] -> Number
; computes a product of all numbers in l

(check-expect
 (product '(1 2 3)) 6)

(define (product lon)
  (foldr * 1 lon))


; With one of the fold functions, you can define a function that
; horizontally composes a list of Images. Also define a function
; that stacks a list of images vertically.

; [List-of Image] -> Image
; stacks the images horizontally

(check-expect
 (beside-from-fold (list (square 20 "solid" "red")
                         (square 50 "solid" "blue")))
 (beside (square 20 "solid" "red")
         (square 50 "solid" "blue")))

(define (beside-from-fold loi)
  (local (; Image Image -> Image
          ; merges two images horizontally
          (define (put-beside old new)
            (overlay/offset old
                            (* 1/2 (+ (image-width new) ; offsets by half of the sum of widths
                                      (image-width old)))
                            0 ; there is no vertical offset
                            new)))
    (foldr put-beside empty-image loi))) 


; [List-of Image] -> Image
; stacks the images vertically

(check-expect
 (above-from-fold (list (square 20 "solid" "red")
                        (square 50 "solid" "blue")))
 (above (square 20 "solid" "red")
        (square 50 "solid" "blue")))

(define (above-from-fold loi)
  (local (; Image Image -> Image
          ; merges two images vertically
          (define (put-above old new)
            (overlay/offset old
                            0 ; no horizontal offset
                            (* 1/2 (+ (image-height new)
                                      (image-height old)))
                            new)))
    (foldr put-above empty-image loi))) ; foldl is also possible, but old and new have to be swapped


;;;;;;;;;;;;;; ex. 273 ;;;;;;;;;;;;;;;;;;;

; The fold functions are so powerful that you can define almost any
; list processing functions with them. Use fold to define map.

; [X Y] [X -> Y] [List-of X] -> [List-of Y]
; applies f to every item on the list

(check-expect
 (map-from-fold add1 '(1 2 3))
 (map add1 '(1 2 3)))

(define (map-from-fold f l)
  (local (; produces an element of new l by applying f
          (define (cons-with-f x y)
            (cons (f x) y)))
    (foldr cons-with-f '() l)))


;;;;;;;;;;;;;; ex. 274 ;;;;;;;;;;;;;;;;;;

; Use existing abstractions to define the prefixes and suffixes functions from
; exercise 190. Ensure that they pass the same tests as the original function.

; LLS is one of:
; - '()
; (cons List-of-1Strings LLS)

; [List-of 1String] -> LLS
; produces all prefixes of LLS

(check-expect
 (prefix '("a" "b" "a"))
 '(("a")
   ("a" "b")
   ("a" "b" "a")))

(define (prefix lo1)
  (local (; helper function
          (define (cut-there n)
            (cut-here n lo1)))
    (build-list (length lo1) cut-there))) ; is there a better way to do it using abstraction?

; Number List -> List
; produces a list of first n elements in l
(define (cut-here n l)
  (cond
    [(zero? n) (cons (first l) '())]
    [else
     (cons (first l)
           (cut-here (sub1 n) (rest l)))]))

; [List-of 1String] -> LLS
; produces all suffixes of lo1

(check-expect
 (suffix '("a" "b" "c"))
 '(("c")
   ("b" "c")
   ("a" "b" "c")))

(define (suffix lo1)
  (local (; helper function
          (define (cut-there n)
            (reverse (cut-here n (reverse lo1)))))
    (build-list (length lo1) cut-there)))

