#lang htdp/isl

; Number -> LLN (list-of-lists-of-number)
; produces an identity matrix
(define (identityM n)
  (local((define col n) ; constant no. of columns
         
         ; Number Number -> LLN
         ; creates n x n matrix
         (define (create-matrix n)
           (cond
             [(zero? n) '()]
             [else (cons (create-row n col)
                         (create-matrix (sub1 n)))]))
         
         ; Number Number -> LLN
         ; creates one row of
         ; an identity matrix
         (define (create-row row col)
           (cond
             [(zero? col) '()]
             [else
              (cons (if (= row col) 1 0)
                    (create-row row (sub1 col)))])))
    
    (create-matrix n)))
   
    

(check-expect (identityM 1) (list (list 1)))
(check-expect (identityM 3) (list (list 1 0 0)
                                  (list 0 1 0)
                                  (list 0 0 1)))

