
; [Number] [List-of Number] [Number -> Number]
; -> [List-of Number]

; [String] [List-of String] [String -> String]
; -> [List-of String]

; [X] [List-of X] [X -> Y] -> [List-of Y]
; situation in which we extract one of the
; values from X seems ambibuous
