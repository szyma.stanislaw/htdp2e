#lang htdp/isl+

(require 2htdp/abstraction)
; because I admit pattern matching is clever here

(define-struct no-info [])
(define NONE (make-no-info))
 
(define-struct node [ssn name left right])
; A BT (short for BinaryTree) is one of:
; – NONE
; – (make-node Number Symbol BT BT)

; examples
(define bt1 (make-node 15 'd
                       NONE
                       (make-node
                        24 'i NONE NONE)))

(define bt2 (make-node 15 'e
                       (make-node
                        87 'h NONE NONE)
                       bt1))

;;;;;;;;;;;;;;;;;;;; ex. 322 ;;;;;;;;;;;;;;;;;;;;;;;;;;

; Design contains-bt?, which determines whether
; a given number occurs in some given BT.

; BT -> Boolean
(check-expect (contains-bt? 53 bt2) #f)
(check-expect (contains-bt? 87 bt2) #t)

(define (contains-bt? n bt)
  (match bt
    [(no-info) #f]
    [(node num sym left right)
     (or (= n num)
         (contains-bt? n left)
         (contains-bt? n right))]))

;;;;;;;;;;;;;;;;;;;; ex. 323 ;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Design search-bt. The function consumes a number n
; and a BT. If the tree contains a node structure whose
; ssn field is n, the function produces the value of
; the name field in that node. Otherwise, the function
; produces #false

; N BT -> Symbol
; produces the FIRST found symbol associated
; with a given ssn
(check-expect (search-bt 53 bt2) #f)
(check-expect (search-bt 15 bt2) 'e)

(define (search-bt n bt)
  (match bt
    [(no-info) #f]
    [(node num sym left right)
     (if (= num n) sym
         (if (contains-bt? n left)
             (search-bt n left)
             (search-bt n right)))]))

; N BT -> [List-of Symbol] or Boolean
; produces a LIST of all names associated
; with a given ssn, else #false
(check-expect (search-bt2 53 bt2) #f)
(check-expect (search-bt2 15 bt2) '(e d))

(define (search-bt2 n bt)
  (local ((define (list-names bt)
            (match bt
              [(no-info) '()]
              [(node num sym left right)
               (local ((define lft (list-names left))
                       (define rght (list-names right)))
                 (if (= n num)
                     (cons sym (append lft rght))
                     (append lft rght)))]))
          
          (define already-listed (list-names bt)))
    
    (if (empty? already-listed) #f
        already-listed)))


;;;;;;;;;;;;;;;;;;;; ex. 324 ;;;;;;;;;;;;;;;;;;;;

; Design the function inorder. It consumes a binary
; tree and produces the sequence of all the ssn numbers
; in the tree as they show up from left to right when
; looking at a tree drawing.

; BT -> [List-of Number]
; produces a list of all ssn's from
; most left to most right
(check-expect (inorder bt1) '(15 24))

(define (inorder bt)
  (match bt
    [(no-info) '()]
    [(node num sym left right)
     (append (inorder left)
             (cons num
                   (inorder right)))]))


;;;;;;;;;;;;;;;;;;;;;;;; ex. 325 ;;;;;;;;;;;;;;;;;;;;;;;

; Design search-bst. The function consumes a number n and
; a BST. If the tree contains a node whose ssn field is n,
; the function produces the value of the name field in that
; node. Otherwise, the function produces NONE.

; BST -> Symbol or NONE
; outputs the symbol associated with a given ssn

(define bst1 (make-node 4 'd
                        (make-node 2 'a NONE NONE)
                        (make-node 7 'f NONE
                                   (make-node 20 'g NONE NONE)))) 

(check-expect (search-bst 7 bst1) 'f)
                          
(define (search-bst n bst)
  (match bst
    [(no-info) NONE]
    [(node num sym left right)
     (if (= n num) sym
         (if (> n num)
             (search-bst n right)
             (search-bst n left)))]))


;;;;;;;;;;;;;;;;;;;;;;;;;; ex. 326 ;;;;;;;;;;;;;;;;;;;;;;;;;;

; Design the function create-bst. It consumes a BST B,
; a number N, and a symbol S. It produces a BST that
; is just like B and that in place of one NONE subtree
; contains the node structure: (make-node N S NONE NONE)

; BST N Sym -> BST
; given a number and a symbol produces a bst
; with a new node in correct place
(check-expect (create-bst 4 'f NONE)
              (make-node 4 'f NONE NONE))
(check-expect (create-bst 4 'f (make-node 5 'a
                                          (make-node 3 'd NONE NONE)
                                          NONE))
              (make-node 5 'a
                         (make-node 3 'd NONE
                                    (make-node 4 'f NONE NONE))
                         NONE))

(define (create-bst n s bst)
  (match bst
    [(no-info) (make-node n s NONE NONE)]
    [(node num sym left right)
     (cond
       [(= n num) bst] ; suppose we do not want duplicates
       [(> n num) (make-node num sym left (create-bst n s right))]
       [(< n num) (make-node num sym (create-bst n s left) right)])]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ex. 327 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Design the function create-bst-from-list. It consumes a list of
; numbers and names and produces a BST by repeatedly applying create-bst.

; [List-of [List-of Number Symbol]] -> BST
(define (create-bst-from-list l)
  (match l
    ['() NONE]
    [(cons pair rst)
     (create-bst (first pair) (second pair)
                 (create-bst-from-list rst))]))

; using existing abstractions
(define (create-bst-from-list2 l)
  (foldr (lambda (x y) (create-bst (first x) (second x) y)) NONE l))
