#lang htdp/isl+

(require 2htdp/abstraction)

(define-struct add [left right])
(define-struct mul [left right])

;; ex. 345

; (+ 10 -10)
(define e1 (make-add 10 -10))

; (+ (* 20 3) 33)
(define e2 (make-add (make-mul 20 3) 33))

; (+ (* 3.14 (* 2 3)) (* 3.14 (* -1 -9)))
(define e3 (make-add (make-mul 3.14 (make-mul 2 3))
                     (make-mul 3.14 (make-mul -1 -9))))

;; ex. 346

; A Number Expr is one of:
; - Number
; - (make-expr Expr Expr)

;; ex. 347

; Expr -> Number
; computes the value of an expr
(check-expect (eval-expr e1) 0)
(check-expect (eval-expr e2) 93)
(check-expect (eval-expr e3) (+ 18.84 28.26))

(define (eval-expr e)
  (match e
    [(add e1 e2) (+ (eval-expr e1)
                    (eval-expr e2))]
    [(mul e1 e2) (* (eval-expr e1)
                    (eval-expr e2))]
    [n n]))

;; ex. 348

(define-struct ande [left right])
(define-struct ore [left right])
(define-struct note [expr])

; BExpr is one of:
; - Boolean
; - (make-expr BExpr BExpr)

(define be (make-ande (make-note #f)
                      (make-ore #t #f)))

; BExpr -> Boolean
(check-expect (eval-bexpr be) #t)

(define (eval-bexpr e)
  (match e
    [(ande e1 e2) (and (eval-bexpr e1)
                       (eval-bexpr e2))]
    [(ore e1 e2) (or (eval-bexpr e1)
                     (eval-bexpr e2))]
    [(note e) (not e)]
    [b b]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; X -> Boolean
; is it an atom?
(define (atom? a)
  (or (symbol? a)
      (string? a)
      (number? a)))

; S-expr -> BSL-expr
(define (parse s)
  (cond
    [(atom? s) (parse-atom s)]
    [else (parse-sl s)]))

; error messages for the parser
(define TOO-SHORT "the expression is too short")
(define WRONG-SYM "this operator is unknown")
(define TOO-LONG "the expression is too long")
(define ONLY-NUMS "Only numbers are accepted")

; SL -> BSL-expr 
(define (parse-sl s)
  (local ((define L (length s)))
    (cond
      [(< L 3) (error TOO-SHORT)]
      [(and (= L 3) (symbol? (first s)))
       (cond
         [(symbol=? (first s) '+)
          (make-add (parse (second s)) (parse (third s)))]
         [(symbol=? (first s) '*)
          (make-mul (parse (second s)) (parse (third s)))]
         [else (error WRONG-SYM)])]
      [else (error TOO-LONG)])))
 
; Atom -> BSL-expr 
(define (parse-atom s)
  (cond
    [(number? s) s]
    [(string? s) (error ONLY-NUMS)]
    [(symbol? s) (error ONLY-NUMS)]))

;; ex. 349

(check-error (parse "you") ONLY-NUMS)
(check-error (parse '(+ 3)) TOO-SHORT)
(check-error (parse '(a 3 3)) WRONG-SYM)
(check-error (parse '(* 3 4 5)) TOO-LONG)

(check-expect (parse 3) 3)
(check-expect (parse '(+ 3 3)) (make-add 3 3))
(check-expect (parse '(* 3 3)) (make-mul 3 3))

;; ex. 351

; S-Expr -> Number
(check-expect (interpreter-expr '(+ 3 4)) 7)
(check-error (interpreter-expr '(+ 3 5 6)) TOO-LONG)

(define (interpreter-expr sexp)
  (eval-expr (parse sexp)))
