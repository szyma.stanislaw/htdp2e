;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname 173_no_articles) (read-case-sensitive #t) (teachpacks ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp") (lib "batch-io.rkt" "teachpack" "2htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp") (lib "batch-io.rkt" "teachpack" "2htdp")) #f)))
; An LLS is one of: 
; – '()
; – (cons Los LLS)
; interpretation a list of lines, each is a list of Strings

; writes a file without any articles
(define (no-articles* f)
  (write-file (string-append "no-articles-" f)
              (no-articles (read-words/line f))))

; input data examples
(define los0 '())
(define los1 (cons "the" (cons "dog" (cons "is" '()))))
(define los2 (cons "I" (cons "am" (cons "a" (cons "no" '())))))

(define lls0 '())
(define lls1 (cons los1 (cons los2 '())))

; LLS -> LLS
; removes the articles (a, an, the) from LLS
(define (no-articles lls)
  (cond
    [(empty? lls) ""]
    [else (string-append (cut-article (first lls))
                         "\n"
                         (no-articles (rest lls)))]))

(check-expect (no-articles lls0) "")
(check-expect (no-articles lls1) "dog is\nI am no\n")

; Los -> Los
; removes the articles from a List-of-strings
(define (cut-article los)
  (cond
    [(empty? los) ""]
    [(or (string=? "a" (first los))
         (string=? "an" (first los))
         (string=? "the" (first los)))
     (cut-article (rest los))]
    [else (string-append (first los) (cond
                                  [(empty? (rest los)) ""]
                                  [else " "])
                    (cut-article (rest los)))]))

(check-expect (cut-article los0) "")
(check-expect (cut-article los1) "dog is")
(check-expect (cut-article los2) "I am no")