#lang htdp/bsl+

; Number List-of-numbers -> Boolean
; searches a sorted list for a given number
(define (search-sorted n l)
  (cond
    [(empty? l) #f]
    [(> n (first (sort> l))) #f]
    [(= n (first (sort> l))) #t]
    [else (search-sorted n (rest (sort> l)))]))
(check-expect (search-sorted 1 '()) #f)
(check-expect (search-sorted 6 (list 7 8 -19 5)) #f) 
(check-expect (search-sorted 6 (list 200 1 10 6 16)) #t)

; List-of-Numbers -> List-of-numbers
; sorts a list in descending order
(define (sort> l)
  (cond
    [(empty? l) l]
    [else (insert (first l) (sort> (rest l)))]))

; Number List-of-Numbers -> List-of-Numbers
; inserts a given number into the list
(define (insert n l)
  (cond
    [(empty? l) (list n)]
    [(>= n (first l)) (cons n l)]
    [else (cons (first l) (insert n (rest l)))]))
