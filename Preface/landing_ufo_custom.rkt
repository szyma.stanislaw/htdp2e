;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-beginner-reader.ss" "lang")((modname landing_ufo_custom) (read-case-sensitive #t) (teachpacks ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp"))) (htdp-settings #(#t constructor repeating-decimal #f #t none #f ((lib "image.rkt" "teachpack" "2htdp") (lib "universe.rkt" "teachpack" "2htdp")) #f)))
;constants
(define WIDTH 200) 
(define HEIGHT 400)
(define ROCKET (overlay (circle 10 "solid" "green")
         (rectangle 40 4 "solid" "green")))
(define BED (rectangle WIDTH 10 "solid" "grey"))

(define ROCKET-CENTER-TO-TOP
  (- HEIGHT (/ (image-height ROCKET) 2) 10))
(define SCENE (empty-scene WIDTH HEIGHT "blue"))

;functions
(define (picture-of-rocket h)
  (cond
    [(<= h ROCKET-CENTER-TO-TOP)
     (place-images (list ROCKET
                         BED)
                   (list (make-posn (/ WIDTH 2) h)
                         (make-posn (/ WIDTH 2) (- HEIGHT 5))) SCENE)]
    
    [(> h ROCKET-CENTER-TO-TOP)
     (place-images (list ROCKET 
                         BED)
                   (list (make-posn (/ WIDTH 2) ROCKET-CENTER-TO-TOP)
                         (make-posn (/ WIDTH 2) (- HEIGHT 5))) SCENE)]))



