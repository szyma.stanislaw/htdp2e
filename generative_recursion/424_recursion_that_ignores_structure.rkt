#lang htdp/isl+

; [List-of Number] -> [List-of Number]
; produces a sorted version of alon
; idea - fetches the first element on the list (pivot)
; and appends it to the list with numbers smaller than
; the pivot and the list with numbers larger than the pivot
(check-expect (quick-sort< '(11 9 2 18)) '(2 9 11 18))
(check-expect (quick-sort< '()) '())
(check-expect (quick-sort< '(11 11 3 51 531)) '(3 11 11 51 531))
; initial assumption was that the numbers on the list are distinct
; meaning that the size of the list above should be reduced (see ex. 428)
              
(define (quick-sort< alon)
  (cond
    [(empty? alon) '()]
    [(empty? (rest alon)) alon] ; reduces the time of computation ex. 426
    [else (local ((define pivot (first alon))
                  ; A list with the numbers equal to the pivot ex. 428
                  (define pivoters
                    (filter (lambda (n) (= n pivot)) alon))
                  ; A list with numbers smaller than the pivot
                  (define smallers
                    (filter (lambda (n) (< n pivot)) alon))
                  ; A list with numbers larger than the pivot
                  (define largers
                    (filter (lambda (n) (> n pivot)) alon)))
            
            (append (quick-sort< smallers)
                    pivoters
                    (quick-sort< largers)))]))

;; ex. 427

; if the length of the list is below
; treshold, uses sort< instead
(define (quicksort2 alon)
  (cond
    [(< (length alon) TRESHOLD) (sort alon <)]
    [else (quick-sort< alon)]))

(define TRESHOLD 100)

;; ex. 430

; [List-of Number] [Number Number -> Boolean]
; -> [List-of Number]
; sorts a list of numbers according to c
(check-expect (my-quicksort '(11 9 2 18) <) '(2 9 11 18))
(check-expect (my-quicksort '(11 9 2 18) >) '(18 11 9 2))
(check-expect (my-quicksort '(11 11 9 2 18) >) '(18 11 11 9 2))

(define (my-quicksort l c)
  (cond
    [(or (empty? l)
         (empty? (rest l))) l]
    [else (local ((define pivot (first l))

                  (define pivoters
                    (filter (lambda (n) (= n pivot)) l))

                  (define smallers
                    (filter (lambda (n) (c n pivot)) l))
                  
                  (define largers
                    (filter (lambda (n) (not (or (= n pivot)
                                                 (c n pivot)))) l)))

            (append (my-quicksort smallers c)
                    pivoters
                    (my-quicksort largers c)))]))






