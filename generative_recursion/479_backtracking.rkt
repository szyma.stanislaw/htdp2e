#lang htdp/isl+

(require 2htdp/image)
(require 2htdp/abstraction)

(define QUEENS 8)
; A QP is a structure:
;   (make-posn CI CI)
; A CI is an N in [0,QUEENS).
; interpretation (make-posn r c) denotes the square at 
; the r-th row and c-th column

;; ex. 479

; Design the threatening? function. It consumes two QPs and
; determines whether queens placed on the two respective
; squares would threaten each other.

; QP QP -> Boolean
; are the queens threatening each other
(check-expect (threatening? (make-posn 3 3) (make-posn 7 3)) #t)
(check-expect (threatening? (make-posn 3 3) (make-posn 7 5)) #f)
(check-expect (threatening? (make-posn 5 2) (make-posn 7 4)) #t)

(define (threatening? p q)
  (local ((define px (posn-x p))
          (define py (posn-y p))
          (define qx (posn-x q))
          (define qy (posn-y q)))

    (or (= px qx)
        (= py qy)
        (= (+ px py) (+ qx qy))
        (= (- px py) (- qx qy)))))

;; ex. 480

; Design render-queens. The function consumes a natural
; number n, a list of QPs, and an Image. It produces an
; image of an n by n chess board with the given image
; placed according to the given QPs.

; N [List-of QP] Image -> Image
; renders a n by n chess board with a given image
; placed according to the positions of queens.

(define (render-queens n loq img)
  (local ((define (tile c) (square SIZE 'solid c))

          ; [X X -> Boolean] X X -> Boolean
          ; does p hold for both x and y
          (define (both? p x y)
            (and (p x) (p y)))

          ; N -> [List-of [List-of Symbol]]
          ; produces a matrix of 'black and 'white
          (define (chessboard n)
            (for/list ([row n])
              (for/list ([col n])
                (if (or (both? odd? row col)
                        (both? even? row col))
                    'white
                    'black))))

          ; [List-of [List-of Symbol]] -> Image
          (define (render-board loc)
            (foldr (lambda (r rst) (above (foldr (lambda (c rst) (beside (tile c) rst))
                                                 empty-image r)
                                          rst))
                   empty-image loc))

          ; [List-of QP] Image -> Image
          ; renders final image on a given background
          (define (queen-render loq background)
            (foldr (lambda (q rst) (place-image img
                                                (* SIZE (+ (posn-x q) 0.5)) 
                                                (* SIZE (+ (posn-y q) 0.5))
                                                rst))
                   background loq)))

    (queen-render loq (render-board (chessboard n)))))

(define SIZE 5) ; size of a chessboard tile in pixels


; N -> [List-of QP] or #false
; finds a solution to the n queens problem 
 
; data example: [List-of QP]
(define 4QUEEN-SOLUTION-2
  (list  (make-posn 0 2) (make-posn 1 0)
         (make-posn 2 3) (make-posn 3 1)))

;; ex. 481

; Design the n-queens-solution? function, which consumes a natural
; number n and produces a predicate on queen placements that
; determines whether a given placement is a solution to an n queens puzzle

; N -> [QP -> Boolean]
(define (n-queens-solution? n)
  (lambda (l) (and (= (length l) n)
                   (for*/and ([qp l] [rst (remove qp l)])
                     (not (threatening? qp rst))))))

; Design the function set=?. It consumes two lists and determines whether
; they contain the same items—regardless of order.

; [List x] [List x] -> Boolean
; are the two lists equal
(check-expect (set=? '(a b c d e f) '(a c b d f e)) #t)

; I have no idea 
(define (set=? sx sy)
  (and (= (length sx) (length sy))
       (andmap (lambda (x) (member? x sy)) sx)))

;; ex. 482

; Board is a [List-of QP]
; contains the list of positions where a queen has been placed;


; Board N -> [Maybe [List-of QP]]
; places n queens on board; otherwise, returns #false
(check-satisfied (place-queens '() 8) (n-queens-solution? 8))

(define (place-queens a-board n)
  (local (; N -> Board 
          ; creates the initial n by n board
          (define (board0 n)
            (for*/list ([row n] [col n])
              (make-posn row col)))
          
          ; Board N -> [List-of QP]
          ; finds spots where it is still safe to place a queen
          (define (find-open-spots a-board)
            (filter (lambda (anyqp) (andmap (lambda (qp) (not (threatening? anyqp qp)))
                                          a-board))
                    (board0 n)))

          ; [List-of QP] -> [Maybe [List-of QP]] 
          (define (try loq)
            (cond
              [(= (length a-board) n) a-board]
              [(empty? loq) #f]
              [else (local ((define candidate
                              (place-queens (cons (first loq) a-board) n)))

                      (if (false? candidate)
                          (try (rest loq))
                          candidate))])))

    (try (find-open-spots a-board))))
